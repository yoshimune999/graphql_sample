"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var express_1 = __importDefault(require("express"));
var apollo_server_express_1 = require("apollo-server-express");
var schema_1 = __importDefault(require("./graphql/schema/schema"));
var port = 3000;
var app = express_1.default();
// The GraphQL endpoint ルーティング
app.use('/graphql', express_1.default.json(), apollo_server_express_1.graphqlExpress({ schema: schema_1.default }));
// GraphiQL, a visual editor for queries
app.use('/graphql_playground', apollo_server_express_1.graphiqlExpress({ endpointURL: '/graphql' }));
// Start the server
app.listen(port, function () {
    console.log('Go to http://localhost:3000/graphql_playground to run queries!');
});
