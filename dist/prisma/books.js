"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.books = void 0;
exports.books = [
    {
        title: "Harry Potter and the Sorcerer's stone",
        author: "J.K. Rowling",
        price: 2000
    },
    {
        title: "Jurassic Park",
        author: "Michael Crichton",
        price: 3000
    }
];
