"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var graphql_tools_1 = require("graphql-tools");
// モックデータ
var books = [
    {
        title: "Harry Potter and the Sorcerer's stone",
        author: "J.K. Rowling",
        price: 2000
    },
    {
        title: "Jurassic Park",
        author: "Michael Crichton",
        price: 3000
    }
];
// GraphQLのスキーマ情報
var typeDefs = "\n    type Query { books: [Book] }\n    type Book { title: String, author: String, price: Int }\n  ";
// resolver(データ処理)の設定
// DBからデータを取得したり、APIを呼び出したりする処理もここで記述
var resolvers = {
    Query: { books: function () { return books; } }
};
// GraphQL の Schema 設定
exports.default = graphql_tools_1.makeExecutableSchema({
    typeDefs: typeDefs,
    resolvers: resolvers
});
