import express from "express";
import  schema  from './graphql/schema/schema';
import { context } from './prisma/context'
import { ApolloServer } from 'apollo-server'

const port = 3000;
const app: express.Express = express();

const server = new ApolloServer({ schema:schema, context: context, playground:true})

server.listen(port).then(({url}) => {
  console.log(`Go to ${url}`);
});