import { PrismaClient } from '@prisma/client'

const prisma = new PrismaClient()
// prisma 定義
export interface Context {
  prisma: PrismaClient
}

export const context: Context = {
  prisma: prisma
}