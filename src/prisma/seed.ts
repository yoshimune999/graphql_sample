import { PrismaClient } from '@prisma/client';
import { authors } from './authors'

const prisma = new PrismaClient()

// seed 実行定義
async function main() {
  for (let author of authors){
    await prisma.author.create({ 
        data: author
    })
  }
}
// seed 実行
main()
  .catch(e => {
    console.error(e);
    process.exit(1);
  })
  .finally(async () => {
    await prisma.$disconnect();
  })