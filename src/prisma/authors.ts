// seed 用データ
export const authors = [
    {
        name: "J.K. Rowling",
        age: 40,
        books: {
            create: [
                {
                    title: "Harry Potter and the Philosopher's stone",
                    price: 2000,
                },
                {
                    title: "Harry Potter and the Chamber of Secrets, Book 2",
                    price: 3000,
                }
            ]
        }
    },
    {
        name: "Michael Crichton",
        age: 50,
        books: {
            create: [
                {
                    title: "Jurassic Park",
                    price: 3000
                }
            ]
        }
    }
]