import { makeExecutableSchema } from "graphql-tools";
import { typeDefs } from '../types/types'
import { resolvers } from '../resolvers/resolvers'
  
// type と resolver を渡して schema 定義
export default makeExecutableSchema({
  typeDefs,
  resolvers
});