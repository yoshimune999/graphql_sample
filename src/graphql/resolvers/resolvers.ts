import { context, Context } from '../../prisma/context'
import { DateTimeResolver } from 'graphql-scalars'

// resolver 定義
export const resolvers = {
  Query: {
    allAuthors: () => {
      return context.prisma.author.findMany({
        include:{
          books:true
        }
      })
    },
    authorById: (parent:any, args:{id: number}, context: Context) => {
      return context.prisma.author.findUnique({
        where: {
            id: args?.id || undefined
          },
        include: {
          books: true,
        }
      })
    },
    allBooks: () => {
      return context.prisma.book.findMany()
    },
    bookById: (parent:any, args:{id: number}, context: Context) => {
      return context.prisma.book.findUnique({
        where: {
          id: args?.id || undefined
        }
      })
    }
  },
  Mutation: {
    addAuthor: (parent:any, args: {data: AuthorCreateInput}, context: Context) => {
      const bookData = args.data.books?.map(book => {
        return { title: book.title, price: book.price}
      })

      return context.prisma.author.create({
        data:{
          name: args?.data.name,
          age: args?.data.age,
          books:{
            create: bookData
          } 
        }
      })
    },
    editAuthor: (parent:any, args: {id: number, data: AuthorCreateInput}, context: Context) => {
      const bookData = args.data.books?.map(book => {
        return { title: book.title, price: book.price}
      })
      return context.prisma.author.update({
        where: {
          id: args?.id
        },
        data: {
          name: args?.data.name,
          age: args?.data.age,
          books:{
            create: bookData
          }
        }
      })
    },
    editBook: (parent:any, args: {id: number, data: BookCreateInput}, context: Context) => {
      return context.prisma.book.update({
        where: {
          id: args?.id || undefined
        },
        data: {
          title: args?.data.title,
          price: args?.data.price,
        }
      })
    },
    deleteAuthor: (parent:any, args: {id: number}, context: Context) => {
      return context.prisma.author.delete({
        where: {
          id: args?.id
        }
      })
    }
  },
  DateTime: DateTimeResolver,
}

interface BookCreateInput {
  title: string,
  price: number,
}

interface AuthorCreateInput {
  name: string,
  age: number,
  books: BookCreateInput[]
}


