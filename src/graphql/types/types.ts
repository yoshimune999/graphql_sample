import { gql } from 'apollo-server'

// type 定義
export const typeDefs = gql`
type Query { 
  allAuthors: [Author]
  authorById(id: Int!): Author
  allBooks: [Book]
  bookById(id: Int!): Book 
}

type Mutation {
  addAuthor(data: AuthorCreateInput!): Author
  editAuthor(id: Int!, data: AuthorCreateInput): Author
  editBook(id: Int!, data: BookCreateInput!): Book
  deleteAuthor(id: Int!): Author
}

type Author {
  id: Int
  createdAt: DateTime!
  name: String
  age: Int
  books: [Book]
}

type Book { 
  id: Int
  createdAt: DateTime!
  updatedAt: DateTime!
  title: String
  price: Int
}

input AuthorCreateInput {
  name: String
  age: Int
  books: [BookCreateInput]!
}

input BookCreateInput {
  title: String
  price: Int
}

scalar DateTime
`;