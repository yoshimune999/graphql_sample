* expressjs + GraphQL + prisma

## 環境構築
```sh
docker-compose up -d
```
## 以下 docker コンテナ内
```sh
npm install
```
```sh
npx prisma migrate dev
```
```sh
npx prisma db seed --preview-feature
```
```sh
npm run dev
```
* [ブラウザでgraphqlをたたく](http://localhost:3000/)
